FROM jupyter/base-notebook
MAINTAINER Jupyter Project <jupyter@googlegroups.com>

USER root

RUN apt-key adv --keyserver keyserver.ubuntu.com --recv-keys FCAE2A0E115C3D8A

RUN echo "deb http://cloud.r-project.org/bin/linux/debian jessie-cran34/" | sudo tee /etc/apt/sources.list.d/docker.list

RUN apt-get update -y

RUN	apt-get install r-base -y

RUN apt-get install vim -y

USER jovyan

# install Python + NodeJS with conda
RUN wget -q https://repo.continuum.io/miniconda/Miniconda3-4.2.12-Linux-x86_64.sh -O /tmp/miniconda.sh  && \
    echo 'd0c7c71cc5659e54ab51f2005a8d96f3 */tmp/miniconda.sh' | md5sum -c - && \
    bash /tmp/miniconda.sh -f -b -p /opt/conda && \
    /opt/conda/bin/conda update conda --yes && \
    /opt/conda/bin/conda create -n ipykernel_py2 python=2 ipykernel
RUN /bin/bash -c "source activate ipykernel_py2" 

RUN	/opt/conda/bin/conda install --yes -c conda-forge python=3.5 pandas numpy scikit-learn seaborn matplotlib pymongo scipy shapely geopy statsmodels openpyxl python-dateutil requests psycopg2 sqlalchemy tornado jinja2 traitlets requests pip configurable-http-proxy && \
    /opt/conda/bin/pip install --upgrade pip && \
    rm /tmp/miniconda.sh
ENV PATH=/opt/conda/bin:$PATH

RUN	python -m ipykernel install --user

RUN R --save << setup.r

VOLUME ~/work:/work

EXPOSE 8888
